def h1(k, m):
    return k % m


def h2(k, m):
    return 1 + (k % (m - 2))


def h(k, m, i):
    k = strToIntConst(k)
    return (h1(k, m) + i * h2(k, m)) % m


def newList(n):
    _list = list()
    for i in range(0, n):
        _list.append(None)
    return _list


def strToIntConst(key):
    const = 111
    if key == '':
        return 0
    if len(key) == 1:
        return ord(key) * const
    result = ord(key[0]) * const + ord(key[1])
    for i in range(2, len(key)):
        result = result * const + ord(key[i])
    return result


def insert(key, array):
    i = 0
    for n in range(0, 5*len(array)):
        index = h(key[1], len(array), i)
        if array[index] is None or array[index] == 'Del':
            array[index] = key
            return
        else:
            i += 1


def delete(key, array):
    i = 0
    for n in range(0, 5 * len(array)):
        index = h(key[1], len(array), i)
        if array[index] == key:
            array[index] = 'Del'
            return
        else:
            i += 1


def __main__(n):
    hash_array = newList(n)
    state = False
    counter = 0
    src = open("nazwiskaASCII.txt", 'r')
    for i in range(0, int(n*0.8)):
        line = src.readline().split(' ')
        line = (line[0], line[1][:-1])
        insert(line, hash_array)
    src.close()
    src = open("nazwiskaASCII.txt", 'r')

    for i in range(0, int(n*0.8)):
        line = src.readline().split(' ')
        if state:
            state = not state
            line = (line[0], line[1][:-1])
            delete(line, hash_array)
        else:
            state = not state
    src.close()
    src = open("nazwiskaASCII.txt", 'r')
    for i in range(0, int(n*0.4)):
        line = src.readline().split(' ')
        line = (line[0], line[1][:-1])
        insert(line, hash_array)

    for i in hash_array:
        if i == 'Del':
            counter += 1
    return n, counter

print(f'Rozmiar tablicy |\tIlość usuniętych elementów')
print(f'{__main__(5003)}')
print(f'{__main__(10007)}')
print(f'{__main__(19997)}')
print(f'------------------')
print(f'{__main__(5000)}')
print(f'{__main__(10000)}')
print(f'{__main__(20000)}')
