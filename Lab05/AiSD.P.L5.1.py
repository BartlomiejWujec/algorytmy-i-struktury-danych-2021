def newHashList(length):
    new = list()
    for i in range(0, length):
        new.append(list())
    return new


def strToIntXOR(key):
    if key == "":
        # raise TypeError("Empty string")
        return 0
    if len(key) % 2 == 0:
        result = 256 * ord(key[0]) + ord(key[1])
        for i in range(2, len(key), 2):
            last = 256 * ord(key[i]) + ord(key[i + 1])
            result = result ^ last
        return result
    result = ord(key[0])
    for i in range(1, len(key), 2):
        last = 256 * ord(key[i]) + ord(key[i + 1])
        result = result ^ last
    return result


def strToIntConst(key):
    const = 111
    if key == '':
        return 0
    if len(key) == 1:
        return ord(key) * const
    result = ord(key[0]) * const + ord(key[1])
    for i in range(2, len(key)):
        result = result * const + ord(key[i])
    return result


def strToIntSlaba(key):
    suma = 0
    for i in key:
        suma += ord(i)
    return suma


def hashing(file, m, function):
    lista = newHashList(m)
    src = open(file, mode='r')
    for i in range(0, 2 * m):
        line = src.readline()
        line = line[0:len(line) - 1]
        line_to_num = function(line)
        line_to_num = line_to_num % m
        lista[line_to_num].append(line)
    return lista


def test(lista):
    longest = 0
    empty = 0
    non_empty = 0
    sum_non_empty = 0
    for i in lista:
        if len(i) == 0:
            empty += 1
        if len(i) > longest:
            longest = len(i)
        if len(i) > 0:
            non_empty += 1
            sum_non_empty += len(i)
    return longest, empty, sum_non_empty / non_empty


D17 = hashing("3700.txt", 17, strToIntConst)
D1031 = hashing("3700.txt", 1031, strToIntConst)
D1024 = hashing("3700.txt", 1024, strToIntConst)
W17 = hashing("3700.txt", 17, hash)
W1031 = hashing("3700.txt", 1031, hash)
W1024 = hashing("3700.txt", 1024, hash)
S17 = hashing("3700.txt", 17, strToIntSlaba)
S1031 = hashing("3700.txt", 1031, strToIntSlaba)
S1024 = hashing("3700.txt", 1024, strToIntSlaba)

print("Najdłuższa", "Puste", "Średnia")
print("Własna dobra")
print(D17)
print(test(D17))
print(test(D1031))
print(test(D1024))
print("==============================")
print("Własna słaba")
print(S17)
print(test(S17))
print(test(S1031))
print(test(S1024))
print("==============================")
print("Wbudowana")
print(W17)
print(test(W17))
print(test(W1031))
print(test(W1024))

# Najdłuższa Puste Średnia
# Własna dobra:
# (5, 3, 2.4285714285714284)
# (7, 147, 2.332579185520362)
# (9, 147, 2.3352337514253136)
# ==============================
# Własna słaba:
# (4, 1, 2.125)
# (13, 325, 2.9206798866855523)
# (13, 325, 2.9298998569384835)
# ==============================
# Wbudowana:
# (5, 3, 2.4285714285714284)
# (7, 140, 2.314253647586981)
# (7, 142, 2.3219954648526078)
#
# P.1: Lepsze wyniki powstay dla tablic długości 1031
# P.2 Słaba funkcja hashująca słabiej rozrzuca klucze podnosząc średnią długość listy
