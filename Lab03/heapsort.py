import math


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n*multiplier + 0.5) / multiplier


def heapify(arr, heapSize, i, x):
    l = 2 * (i - x) + 1 + x
    r = 2 * (i - x) + 2 + x
    if l < heapSize and arr[l] < arr[i]:
        maxVal = l
    else:
        maxVal = i
    if r < heapSize and arr[r] < arr[maxVal]:
        maxVal = r
    if maxVal != i:
        arr[i], arr[maxVal] = arr[maxVal], arr[i]
        heapify(arr, heapSize, maxVal, x)
    return arr


def buildHeap(arr, x, y):
    heapSize = y
    last = (y + x - 2) / 2
    last = int(round_half_up(last))
    for i in range(last, x - 1, -1):
        heapify(arr, heapSize, i, x)
    return arr


def heapSort(arr, x, y):
    arr = buildHeap(arr, x, y)
    heapSize = y
    for i in range(y - 1, x, -1):
        arr[x], arr[heapSize - 1] = arr[heapSize - 1], arr[x]
        heapSize -= 1
        heapify(arr, heapSize, x, x)
    return arr