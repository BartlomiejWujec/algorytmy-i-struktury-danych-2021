class Node:
    def __init__(self, x, measure):
        self.key = x
        self.measure = measure
        self.left = None
        self.right = None
        self.parent = None


class Date:
    def __init__(self, day, month):
        if 1 > day > 31:
            raise Exception("day out of range")
        if 1 > month > 12:
            raise Exception("month out of range")

        self.day = day
        self.month = month

    def __str__(self):
        return f"Date({str(self.day).zfill(2)}-{str(self.month).zfill(2)})"

    def compare(self, date):
        if not isinstance(date, Date):
            raise Exception("Not a date")
        if self.month < date.month:
            return True
        elif self.month > date.month:
            return False
        else:
            if self.day > date.day:
                return False
            else:
                return True


class BST:
    def __init__(self):
        self.root = None

    def insert(self, value, measure):
        node = Node(value, measure)
        root = self.root
        parent = None
        while root is not None:
            parent = root
            if node.key.compare(root.key):
                root = root.left
            else:
                root = root.right
        node.parent = parent
        if parent is None:
            self.root = node
        else:
            if node.key.compare(parent.key):
                parent.left = node
            else:
                parent.right = node

    def search(self, value):
        x = self.root
        while x is not None:
            if x.key == value:
                return x
            if value < x.key:
                x = x.left
            else:
                x = x.right
        return None

    @staticmethod
    def minimum(node):
        if node is None:
            return None
        result = node
        while node.left is not None:
            if node.key < result.key:
                result = node
        return result

    def remove(self, value):
        to_remove = self.search(value)
        if to_remove.left is None and to_remove.right is None:
            if to_remove == self.root:
                self.root = None
            else:
                if to_remove == to_remove.parent.left:
                    to_remove.parent.left = None
                else:
                    to_remove.parent.right = None
        elif to_remove.left is not None and to_remove.right is not None:
            _min = self.minimum(to_remove.right)
            to_remove.key = _min.key
            self.remove(_min)
        else:
            if to_remove.left is not None:
                to_remove.left.parent = to_remove.parent
                if to_remove == self.root:
                    self.root = to_remove.left
                else:
                    if to_remove == to_remove.parent.left:
                        to_remove.parent.left = to_remove.left
                    else:
                        to_remove.parent.right = to_remove.left
            else:
                to_remove.right.parent = to_remove.parent
                if to_remove == self.root:
                    self.root = to_remove.right
                else:
                    if to_remove == to_remove.parent.left:
                        to_remove.parent.left = to_remove.left
                    else:
                        to_remove.parent.right = to_remove.left

    def print(self, x):
        if x is None:
            return
        self.print(x.left)
        print(f"{x.key}, {x.measure}", end=" | ")
        self.print(x.right)

    def printPretty(self, x, indent="", boolean=False):
        if x is None:
            if boolean:
                print(f"{indent}L: ")
            else:
                print(f"{indent}R: ")
            return
        if x == self.root:
            print(f"{indent}{self.root.key}")
            self.printPretty(x.left, "├─", True)
            self.printPretty(x.right, "└─", False)
        else:
            # print(f"{indent}{x.key}")
            if boolean:
                print(f"{indent}L: {x.key}")
                indent = indent[0:-2]
                indent = indent + "│ "
            else:
                print(f"{indent}R: {x.key}")
                indent = indent[0:-2]
                indent = indent + "  "
            self.printPretty(x.left, indent + "├─", True)
            self.printPretty(x.right, indent + "└─", False)

    def merge(self, root_of_tree):
        if root_of_tree is None:
            return
        self.merge(root_of_tree.left)
        self.insert(root_of_tree.key, root_of_tree.measure)
        self.merge(root_of_tree.right)

    # def printRange(self, date1, date2):
    #     if date1.compare(date2):
    #         return
    #     x = self.root
    #     while x.key.compare(date2):
    #         if not x.key.compare(date1):
    #             print(f"{x.key}, {x.measure}")


tree1 = BST()
tree1.insert(Date(3, 3), 123)
tree1.insert(Date(8, 3), 120)
tree1.insert(Date(6, 3), 118)
tree1.printPretty(tree1.root)

tree2 = BST()
tree2.insert(Date(15, 6), 80)
tree2.insert(Date(17, 6), 80)
tree2.insert(Date(18, 6), 79)
tree2.printPretty(tree2.root)

tree1.merge(tree2.root)
tree1.printPretty(tree1.root)