import math
from timeit import default_timer as timer
import random


def round_half_up(n, decimals=0):
    multiplier = 10 ** decimals
    return math.floor(n*multiplier + 0.5) / multiplier


def heapifyAsc(arr, heapSize, i):
    l = 2 * i + 1
    r = 2 * i + 2
    if l < heapSize and arr[l] < arr[i]:
        maxVal = l
    else:
        maxVal = i
    if r < heapSize and arr[r] < arr[maxVal]:
        maxVal = r
    if maxVal != i:
        arr[i], arr[maxVal] = arr[maxVal], arr[i]
        heapifyAsc(arr, heapSize, maxVal)
    return arr


def heapifyDsc(arr, heapSize, i):
    l = 2 * i + 1
    r = 2 * i + 2
    if l < heapSize and arr[l] < arr[i]:
        maxVal = l
    else:
        maxVal = i
    if r < heapSize and arr[r] < arr[maxVal]:
        maxVal = r
    if maxVal != i:
        arr[i], arr[maxVal] = arr[maxVal], arr[i]
        heapifyDsc(arr, heapSize, maxVal)
    return arr


def buildHeapAsc(arr):
    heapSize = len(arr)
    last = (heapSize - 2) / 2
    last = int(round_half_up(last))
    for i in range(last, -1, -1):
        heapifyAsc(arr, heapSize, i)
    return arr


def buildHeapDsc(arr):
    heapSize = len(arr)
    last = (heapSize - 2) / 2
    last = int(round_half_up(last))
    for i in range(last, -1, -1):
        heapifyDsc(arr, heapSize, i)
    return arr


def heapSortAsc(arr):
    arr = buildHeapAsc(arr)
    heapSize = len(arr)
    for i in range(len(arr), -1, -1):
        arr[0], arr[heapSize - 1] = arr[heapSize - 1], arr[0]
        heapSize -= 1
        heapifyAsc(arr, heapSize, i)
    return arr


def heapSortDsc(arr):
    arr = buildHeapDsc(arr)
    heapSize = len(arr)
    for i in range(len(arr), -1, -1):
        arr[0], arr[heapSize - 1] = arr[heapSize - 1], arr[0]
        heapSize -= 1
        heapifyDsc(arr, heapSize, i)
    return arr


# def round_half_up(n, decimals=0):
#     multiplier = 10 ** decimals
#     return math.floor(n*multiplier + 0.5) / multiplier


def heapify(arr, heapSize, i, x):
    l = 2 * (i - x + 1) + x
    r = 2 * (i - x + 1) + 1 + x
    if l < heapSize and arr[l] < arr[i]:
        maxVal = l
    else:
        maxVal = i
    if r < heapSize and arr[r] < arr[maxVal]:
        maxVal = r
    if maxVal != i:
        arr[i], arr[maxVal] = arr[maxVal], arr[i]
        heapify(arr, heapSize, maxVal, x)
    return arr


def buildHeap(arr, x, y):
    heapSize = y
    last = (y + x - 4) / 2
    last = int(round_half_up(last))
    for i in range(last, x - 2, -1):
        heapify(arr, heapSize, i, x)
    return arr


def heapSort(arr, x, y):
    arr = buildHeap(arr, x, y)
    heapSize = y
    for i in range(y - 1, x - 2, -1):
        arr[x - 1], arr[heapSize - 1] = arr[heapSize - 1], arr[x - 1]
        heapSize -= 1
        heapify(arr, heapSize, x - 1, x)
    return arr


# def read(nazwa):
#     src = open(nazwa, mode="r")
#     start = int(src.readline())
#     end = int(src.readline())
#     arr = []
#     line = src.readline()
#     while line != "":
#         arr.append(int(line))
#         line = src.readline()
#     return arr, start, end

# result = read("plik.txt")
def same(n):
    arr = []
    for i in range(0, n):
        arr.append(random.randint(0, 10))
    return arr


def asc(n):
    arr = []
    for i in range(0, n):
        arr.append(random.randint(0, 1000))
    return heapSortAsc(arr)


def dsc(n):
    arr = []
    for i in range(0, n):
        arr.append(random.randint(0, 1000))
    return heapSortDsc(arr)

a = same(10)
print(heapSort(a, 0, len(a)-1))
# for n in range(1000, 9001, 100):
#     arr = asc(n)
#     start = timer()
#     heapSort(arr, 1, len(arr))
#     stop = timer()
#     Tn = stop - start
#     Fn = n
#     print(n, '\t', format(Tn, '.8f'), '\t', int(Fn / Tn))
