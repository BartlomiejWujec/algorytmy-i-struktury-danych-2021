class Node:
    def __init__(self, s):
        self.key = (s, len(s))
        self.next = None
        self.prev = None


class LinkedList:
    def __init__(self):
        self.head = Node("")
        self.head.key = None
        self.head.prev = self.head
        self.head.next = self.head
        self.dummy = self.head

    def insert(self, k):
        if isinstance(k, tuple):
            for i in k:
                if isinstance(i, str):
                    x = Node(i)
                    x.next = self.head
                    x.prev = self.dummy
                    self.head.prev = x
                    self.dummy.next = x
                    self.head = x
                else:
                    raise Exception("Unexpected type")
        else:
            if isinstance(k, str):
                x = Node(k)
                x.next = self.head
                x.prev = self.dummy
                self.head.prev = x
                self.dummy.next = x
                self.head = x
            else:
                raise Exception("Unexpected type")

    def print(self):
        x = self.head
        if x == self.dummy:
            print("List()")
            return
        print("LinkedList(", end="")
        while x.next != self.dummy:
            print(x.key, end=", ")
            x = x.next
        print(x.key, end=")\n")

    def search(self, k):
        x = self.head
        while x != self.dummy and x.key[0] != k:
            x = x.next
        if x != self.dummy:
            return x
        return None

    def remove(self, k):
        x = self.search(k)
        if x is None:
            return
        if x != self.dummy:
            if x == self.head:
                x.prev.next = x.next
                x.next.prev = x.prev
                self.head = x.next
            else:
                x.prev.next = x.next
                x.next.prev = x.prev

    def withoutCopy(self):
        x = self.head
        copies = {}
        while x != self.dummy:
            if x.key[0] not in copies:
                copies[x.key[0]] = 0
            copies[x.key[0]] += 1
            x = x.next
        no_copy = LinkedList()
        for key in copies:
            no_copy.insert(key)
        return no_copy


def mergeList(x1, x2):
    x1.head.prev = x2.dummy
    x2.dummy.next = x1.head

    x1.dummy.prev.next = x2.head
    x2.head.prev = x1.dummy.prev

    x1.dummy = x2.dummy
    merged = x1
    return merged
l = LinkedList()
l.insert(("pies", "kot", "kaczka", "żyrafa", "kot", "pies", 'kot'))
l.print()
no_copy = l.withoutCopy()
no_copy.print()
l.print()
# print(l.search("kot"))
# k = l.withoutCopy()
# k.print()
# l.remove("pas")
# l.remove("pies")
# l.print()
# del l
# del k
# l = LinkedList()
# l.insert(("pies", "kot", "kaczka", "żyrafa", "kot", "pies", 'kot'))
# k = LinkedList()
# k.insert(("drzewo", "rzeka", "kamień", "góra"))
# s = mergeList(l, k)
# del l
# del k
# s.print()
